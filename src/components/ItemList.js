import React, { useState, useEffect  } from 'react';
import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import axios from "../utils/AxiosConfig";
import { formatDate } from "../utils/Utils";

function ItemList() {

  const [items, setItems] = useState([]);

  useEffect(() => {
    axios.get('items')
      .then(function (response) {
        setItems(response.data.content);
        // handle success
        console.log(response);
        console.log(response.data.content);
      })
      .catch(function (error) {
        setItems([]);
        // handle error
        console.log(error);
      });
  }, []);
  
  return (
    <>
      <Container className="wrapperTable">
        <Table striped>
          <thead>
            <tr>
              <th>Code</th>
              <th>Name</th>
              <th>Date</th>
              <th>Stock</th>
              <th>Prices</th>
            </tr>
          </thead>
          <tbody>
            {
              items.map((item, index) => {
                return <tr key={index}>
                  <td>{item.code}</td>
                  <td>{item.name}</td>
                  <td>{formatDate(item.insertDate)}</td>
                  <td>{item.stock}</td>
                  <td>Prices</td>
                </tr>
              })
            }
          </tbody>
        </Table>
      </Container>
    </>
  );
}

export default ItemList;
