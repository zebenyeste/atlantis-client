import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {VIEW_LIST, VIEW_WEEKLY, VIEW_SAVE } from '../constants/Constants';

function Header(props) {

    const { setView } = props;

    return (
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">API ecommerce</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link onClick={() => setView(VIEW_LIST)}>List of items</Nav.Link>
              <Nav.Link onClick={() => setView(VIEW_WEEKLY)}>Best items selling weekly</Nav.Link>
              <Nav.Link onClick={() => setView(VIEW_SAVE)}>Save invoice</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
  
export default Header;