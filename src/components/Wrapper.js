import ItemList from "./ItemList";
import ItemWeekly from "./ItemWeekly";
import InvoiceSave from "./InvoiceSave";
import {VIEW_LIST, VIEW_WEEKLY, VIEW_SAVE } from '../constants/Constants';

function Wrapper(props) {

    const { view } = props; 

    return (
      <>
        {view === VIEW_LIST && <ItemList/>}
        {view === VIEW_WEEKLY && <ItemWeekly/>}
        {view === VIEW_SAVE && <InvoiceSave/>}
      </>
    );
}
  
export default Wrapper;