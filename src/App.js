import React, { useState } from 'react';
import Header from './components/Header';
import Wrapper from './components/Wrapper';
import './App.css';

function App() {

  const [view, setView] = useState("");

  return (
    <div className="App">
      <Header setView={setView}/>
      <Wrapper view={view}/>
    </div>
  );
}

export default App;
